package tictactoe;

/** "Player vs. Player", "Player vs. AI Stupid", "Player vs. AI Easy", "AI Stupid vs. AI Stupid", "AI Stupid vs. AI Easy", "AI Easy vs. AI Easy" */
public enum GameType {
    PLAYER_PLAYER,
    PLAYER_STUPID,
    PLAYER_EASY,
    STUPID_STUPID,
    STUPID_EASY,
    EASY_EASY,
    SIMULATOR_EASY,
}
