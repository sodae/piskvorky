package tictactoe;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

public class Cell
{
    private int x, y;

    private CellType type = CellType.EMPTY;

    public Cell(int x, int y) {
        this.x = x;
        this.y = y;
        this.setType(CellType.EMPTY);
    }

    public boolean isEmpty()
    {
        return this.type == CellType.EMPTY;
    }

    public CellType getType() {
        return type;
    }

    public void setType(CellType type) {
        this.type = type;
    }

    public int getCellX() {
        return x;
    }

    public int getCellY() {
        return y;
    }
}
