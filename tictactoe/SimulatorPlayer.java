package tictactoe;

import tictactoe.sodae.BaseRobotPlayer;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimulatorPlayer extends BaseRobotPlayer {

    public SimulatorPlayer(String name) {
        super(name);
    }

    private int i = 0;

    @Override
    public Point nextMove(Point move) {
        return getMoves().get(i++);
    }

    protected List<Point> getMoves() {
        String log = "Tah AI Easy X: (x:14.0, y:8.0)\n" +
                "Tah Hráč O: (x:13.0, y:7.0)\n" +
                "Tah AI Easy X: (x:13.0, y:8.0)\n" +
                "Tah Hráč O: (x:12.0, y:8.0)\n" +
                "Tah AI Easy X: (x:15.0, y:8.0)\n" +
                "Tah Hráč O: (x:16.0, y:8.0)\n" +
                "Tah AI Easy X: (x:11.0, y:9.0)\n" +
                "Tah Hráč O: (x:12.0, y:9.0)\n" +
                "Tah AI Easy X: (x:12.0, y:7.0)\n" +
                "Tah Hráč O: (x:11.0, y:6.0)\n" +
                "Tah AI Easy X: (x:14.0, y:9.0)\n" +
                "Tah Hráč O: (x:13.0, y:10.0)\n" +
                "Tah AI Easy X: (x:15.0, y:10.0)\n" +
                "Tah Hráč O: (x:16.0, y:11.0)\n" +
                "Tah AI Easy X: (x:14.0, y:7.0)\n" +
                "Tah Hráč O: (x:14.0, y:6.0)\n" +
                "Tah AI Easy X: (x:14.0, y:10.0)\n" +
                "Tah Hráč O: (x:14.0, y:11.0)\n" +
                "Tah AI Easy X: (x:11.0, y:8.0)\n" +
                "Tah Hráč O: (x:13.0, y:6.0)\n" +
                "Tah AI Easy X: (x:12.0, y:6.0)\n" +
                "Tah Hráč O: (x:12.0, y:5.0)\n" +
                "Tah AI Easy X: (x:15.0, y:12.0)\n" +
                "Tah Hráč O: (x:15.0, y:11.0)\n" +
                "Tah AI Easy X: (x:13.0, y:11.0)\n" +
                "Tah Hráč O: (x:14.0, y:12.0)\n" +
                "Tah AI Easy X: (x:15.0, y:5.0)\n" +
                "Tah Hráč O: (x:16.0, y:10.0)\n" +
                "Tah AI Easy X: (x:16.0, y:9.0)\n" +
                "Tah Hráč O: (x:17.0, y:9.0)\n" +
                "Tah AI Easy X: (x:15.0, y:7.0)\n" +
                "Tah Hráč O: (x:13.0, y:13.0)";

        List<Point> titles = new ArrayList<Point>();
        Matcher matcher = Pattern.compile("Tah Hráč O: \\(x:(\\d+).0, y:(\\d+).0\\)").matcher(log); // "\Tah Hráč O: (x:(\d+).0, y:(d+).0)\n"
        while(matcher.find()){
            titles.add(new Point(Integer.valueOf(matcher.group(1)), Integer.valueOf(matcher.group(2))));
        }
        return titles;
    }

}
