package tictactoe;

import tictactoe.sodae.EasyRobotPlayer;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

public class MainFrame extends javax.swing.JFrame  {

    private Game game;

    private JCell labels[][];


    /**
     * Creates new form MainFrame
     */
    public MainFrame(Game game) {
        this.game = game;

        initComponents();
        labels = new JCell[game.getWidth()][getHeight()];
        for (int i = 0; i < game.getWidth(); i++) {
            for (int j = 0; j < game.getHeight(); j++) {
                labels[i][j] = new JCell(game.getCell(i, j));
                labels[i][j].addMouseListener(new java.awt.event.MouseAdapter() {
                    public void mouseClicked(java.awt.event.MouseEvent evt) {
                        labelMouseClicked(evt);
                    }
                });
                mainPanel.add(labels[i][j]);
            }
        }
    }

    public void log(String text) {
        logArea.append(text + "\n");
        logArea.setCaretPosition(logArea.getDocument().getLength());
    }

    public void updateGame()
    {
        for (int i = 0; i < game.getWidth(); i++) {
            for (int j = 0; j < game.getHeight(); j++) {
                labels[i][j].update();
            }
        }
    }

    public void winner(IPlayer player) {
        JOptionPane.showMessageDialog(this, "Vítěz " + player.getName());
    }

    public void onDraw() {
        JOptionPane.showMessageDialog(this, "Remíza");
    }

    public void onBadMove() { JOptionPane.showMessageDialog(this, "Špatný tah"); }

    Thread thread = null;



    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {

        GameType mode = (GameType) playModeSelectBox.getSelectedItem();
        if (mode == GameType.PLAYER_EASY) {
            game.setPlayerCircle(new UserPlayer("Hráč O"));
            game.setPlayerCross(new EasyRobotPlayer("AI Easy X"));
        } else if (mode == GameType.PLAYER_STUPID) {
            game.setPlayerCircle(new UserPlayer("Hráč O"));
            game.setPlayerCross(new StupidPlayer("AI Stupid X"));
        } else if (mode == GameType.STUPID_EASY) {
            game.setPlayerCircle(new StupidPlayer("AI Stupid O"));
            game.setPlayerCross(new EasyRobotPlayer("AI Easy X"));
        } else if (mode == GameType.STUPID_STUPID) {
            game.setPlayerCircle(new StupidPlayer("AI Stupid O"));
            game.setPlayerCross(new StupidPlayer("AI Stupid X"));
        } else if (mode == GameType.EASY_EASY) {
            game.setPlayerCircle(new EasyRobotPlayer("AI Easy O"));
            game.setPlayerCross(new EasyRobotPlayer("AI Easy X"));
        } else if (mode == GameType.SIMULATOR_EASY) {
            game.setPlayerCircle(new SimulatorPlayer("AI Easy O"));
            game.setPlayerCross(new EasyRobotPlayer("AI Easy X"));
        } else { //  (mode == GameType.PLAYER_PLAYER )
            game.setPlayerCircle(new UserPlayer("Hráč O"));
            game.setPlayerCross(new UserPlayer("Hráč X"));
        }

        if (thread != null) {
            thread.interrupt();
            thread.stop(); // I know :D
        }

        thread = new Thread(game);
        thread.start();
    }

    private void playModeSelectBoxActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void labelMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getSource().getClass() != JCell.class) {
            return;
        }
        JCell cell = (JCell) evt.getSource();

        if (game.getOnSlot().getClass() == UserPlayer.class) {
            ((UserPlayer) game.getOnSlot()).clicked(new Point(cell.getCell().getCellX(), cell.getCell().getCellY()));

        }
    }



    /****** DESIGN ******/

    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea logArea;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JComboBox playModeSelectBox;
    private javax.swing.JToggleButton startButton;


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

        startButton = new javax.swing.JToggleButton();
        mainPanel = new javax.swing.JPanel();
        playModeSelectBox = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        logArea = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        startButton.setText("Start");
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });

        mainPanel.setLayout(new java.awt.GridLayout(20, 20));

        HashMap<GameType, String> gameMode = new HashMap<GameType, String>();
        gameMode.put(GameType.PLAYER_PLAYER, "Player vs. Player");
        gameMode.put(GameType.PLAYER_STUPID, "Player vs. AI Stupid");
        gameMode.put(GameType.PLAYER_EASY, "Player vs. AI Easy");
        gameMode.put(GameType.STUPID_STUPID, "AI Stupid vs. AI Stupid");
        gameMode.put(GameType.STUPID_EASY, "AI Stupid vs. AI Easy");
        gameMode.put(GameType.EASY_EASY, "AI Easy vs. AI Easy");
        gameMode.put(GameType.SIMULATOR_EASY, "Simulator vs. AI Easy");

        playModeSelectBox.setModel(new javax.swing.DefaultComboBoxModel(gameMode.keySet().toArray())); // :((
        playModeSelectBox.setActionCommand("playModeChanged");
        playModeSelectBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                playModeSelectBoxActionPerformed(evt);
            }
        });

        logArea.setEditable(false);
        logArea.setColumns(20);
        logArea.setRows(5);
        jScrollPane1.setViewportView(logArea);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        getContentPane().setPreferredSize(new Dimension(700, 500));
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(playModeSelectBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(startButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jScrollPane1))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(startButton)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(playModeSelectBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 223, Short.MAX_VALUE)))
                                .addContainerGap())
        );

        pack();
    }

}
