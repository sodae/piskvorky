package tictactoe;


import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class Game implements Runnable {

    private int WIDTH = 20;
    private int HEIGHT = 20;
    private int WINNING = 5;

    private Cell[][] cells;

    private IPlayer playerCross;
    private IPlayer playerCircle;

    private IPlayer onSlot;


    private List<Event> eventListeners;

    public Game(int WIDTH, int HEIGHT, int WINNING) {
        this.WIDTH = WIDTH;
        this.HEIGHT = HEIGHT;
        this.WINNING = WINNING;

        cells = new Cell[WIDTH][HEIGHT];
        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
                cells[i][j] = new Cell(i, j);
            }
        }
        eventListeners = new ArrayList<Event>();
    }

    public Game() {
        this(20, 20, 5);
    }

    public int getWidth() {
        return WIDTH;
    }

    public int getHeight() {
        return HEIGHT;
    }

    public Cell[][] getCells() {
        return cells;
    }

    public Cell getCell(int x, int y) {
        return cells[x][y];
    }

    public void registerEventListener(Event eventListener) {
        this.eventListeners.add(eventListener);
    }

    public void reset()
    {
        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
                cells[i][j].setType(CellType.EMPTY);
            }
        }
        for (Event eventListener : eventListeners) {
            eventListener.onReset(this);
        }
    }

    public void setPlayerCircle(IPlayer playerCircle) {
        this.playerCircle = playerCircle;
    }

    public void setPlayerCross(IPlayer playerCross) {
        this.playerCross = playerCross;
    }

    public IPlayer getOnSlot() {
        return onSlot;
    }

    public void run()
    {
        this.reset();
        if (onSlot == null) onSlot = playerCross;

        for (Event eventListener : eventListeners) {
            eventListener.onStart(this);
        }

        Point move = null;
        int i = 0;
        while (true) {

            if (Thread.currentThread().isInterrupted()) {
                return;
            }

            i++;
            try {

                move = onSlot.nextMove(move);
                Cell cell = cells[move.x][move.y];
                if (!cell.isEmpty()) {
                    throw new InvalidMoveException(move);
                }

                cell.setType(onSlot == playerCircle ? CellType.CIRCLE : CellType.CROSS);

                for (Event eventListener : eventListeners) {
                    eventListener.onMove(this, onSlot, move);
                }

                if (checkWin(move.x, move.y)) {
                    for (Event eventListener : eventListeners) {
                        eventListener.onFinish(this, onSlot);
                    }
                    onSlot = null;
                    return;
                } else if (isDraw()) {
                    for (Event eventListener : eventListeners) {
                        eventListener.onDraw(this);
                    }
                    onSlot = null;
                    return;
                }

                try { Thread.sleep(200); } catch (InterruptedException e) {}

                onSlot = playerCircle == onSlot ? playerCross : playerCircle;
            } catch (InvalidMoveException e) {
                for (Event eventListener : eventListeners) {
                    eventListener.onBadMove(this, onSlot, e.getPoint());
                }
            }

        }

    }

    /**
     * Remíza
     */
    public boolean isDraw()
    {
        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
                if (cells[i][j].getType() == CellType.EMPTY) return false;
            }
        }
        return true;
    }

    private int getStringLength(CellType type, int startX, int startY, int moveLeft, int moveDown) {

        if (startX < 0 || startY < 0 || startX >= WIDTH || startY >= HEIGHT) return 0;
        Cell cell = cells[startX][startY];
        if (cell.getType() != type) return 0;

        return getStringLength(type, startX+moveLeft, startY+moveDown, moveLeft, moveDown) + 1;
    }

    private boolean checkWin(int x, int y) {
        Cell cell = cells[x][y];
        if (cell.getType() == CellType.EMPTY) return false;
        if (getStringLength(cell.getType(), x, y, -1, -1) + getStringLength(cell.getType(), x, y, 1, 1) - 1 == WINNING) { // check \
            return true;
        }
        if (getStringLength(cell.getType(), x, y, -1, 1) + getStringLength(cell.getType(), x, y, 1, -1) - 1 == WINNING) { // check /
            return true;
        }
        if (getStringLength(cell.getType(), x, y, 0, 1) + getStringLength(cell.getType(), x, y, 0, -1) - 1 == WINNING) { // check |
            return true;
        }
        if (getStringLength(cell.getType(), x, y, 1, 0) + getStringLength(cell.getType(), x, y, -1, 0) - 1 == WINNING) { // check -
            return true;
        }

        return false;
    }

    static  class Event {
        public void onStart(Game game) {}
        public void onFinish(Game game, IPlayer winner) {}
        public void onDraw(Game game) {}
        public void onBadMove(Game game, IPlayer player, Point move) {}
        public void onReset(Game game) {}
        public void onMove(Game game, IPlayer player, Point move) {}
    }

    class InvalidMoveException extends Exception
    {
        Point point;

        InvalidMoveException(Point point) {
            this.point = point;
        }

        public Point getPoint() {
            return point;
        }
    }

}
