package tictactoe.sodae;


import tictactoe.CellType;
import tictactoe.IPlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

abstract public class BaseRobotPlayer implements IPlayer {

    protected int WIDTH = 20;
    protected int HEIGHT = 20;

    protected MapCell map;

    protected final CellType my = CellType.CIRCLE;
    protected final CellType opposite = CellType.CROSS;

    protected String name;

    public BaseRobotPlayer() {
        this("BaseRobotPlayer");
    }

    public BaseRobotPlayer(String name) {
        this.name = name;
        map = new MapCell(WIDTH, HEIGHT);
    }

    @Override
    public void clean()
    {
        map = new MapCell(WIDTH, HEIGHT);
    }

    @Override
    public String getName() {
        return name;
    }

    public static int randInt(int min, int max) {
        return (new Random()).nextInt((max - min) + 1) + min;
    }

    protected class MapCell {
        private CellType cells[][];

        public int value[][];

        public int width = 20;
        public int height = 20;

        MapCell(MapCell mapCell) {
            this(mapCell.width, mapCell.height, mapCell);
        }

        MapCell(int width, int height) {
            this(width, height, null);
        }

        MapCell(int width, int height, MapCell clone) {
            this.width = width;
            this.height = height;
            cells = new CellType[width][height];
            value = new int[width][height];
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    if (clone != null) {
                        cells[i][j]= clone.cells[i][j];
                    } else {
                        cells[i][j] = CellType.EMPTY;
                    }
                    value[i][j] = 0;
                }
            }
        }

        public boolean isEmpty(int x, int y)
        {
            return cells[x][y] == CellType.EMPTY;
        }

        public CellType getType(int x, int y) {
            return cells[x][y];
        }

        public void setType(int x, int y, CellType type) {
            cells[x][y] = type;
        }

        public void setType(double x, double y, CellType type) {
            this.setType((int) x, (int) y, type);
        }
    }


    protected class Analyzer
    {
        protected MapCell map;

        protected CellType enemy;
        protected CellType my;

        protected int MAP_ROW;

        public Analyzer(MapCell map, CellType enemy, CellType my, int MAP_ROW) {
            this.map = map;
            this.enemy = enemy;
            this.my = my;
            this.MAP_ROW = MAP_ROW;
        }

        protected int[] mapRow(int[] row, int startX, int startY, int moveLeft, int moveDown) {
            if (moveLeft == 0 && moveDown == 0) return row;
            for (int i = 0; i < MAP_ROW; i++) {
                int x = startX + (moveLeft*i);
                int y = startY + (moveDown*i);
                if (!(x < 0 || y < 0 || x >= map.width || y >= map.height)) {
                    if (map.getType(x, y) == my) {
                        row[i] = -1;
                    } else if (map.getType(x, y) == enemy) {
                        row[i] = 1;
                    }
                }
            }

            return row;
        }


        public class RowAnalyze {
            public int offset;
            public int score;
            public int length;
        }

        public List<RowAnalyze> analyzeRow(int[] row) {
            List<RowAnalyze> p = new ArrayList<RowAnalyze>();
            RowAnalyze act = null;

            for (int i = 0; i < MAP_ROW; i++) {
                if (row[i] == 1) {
                    if (act == null) {
                        act = new RowAnalyze();
                        p.add(act);
                        act.score = 1;
                        act.offset = i;
                        act.length = 1;
                    } else {
                        act.score++;
                        act.length++;
                    }

                    int remain = 0;
                    for (int uu = act.offset+act.length; uu < MAP_ROW; uu++) {
                        remain = remain+(row[uu]*row[uu]); // always positive
                    }
                    if (remain == 0)
                        return p;

                } else if (row[i] == -1) {
                    act = null;
                } else {
                    if (act != null)
                        act.length++;
                }
            }

            return p;
        }

    }


    protected void printMap()
    {
        for (int i = 0; i < map.width; i++) {
            for (int j = 0; j < map.height; j++) {
                if (map.getType(i,j) == CellType.CIRCLE) {
                    System.out.print("O");
                } else if (map.getType(i,j) == CellType.CROSS) {
                    System.out.print("X");
                } else {
                    System.out.print(" ");
                }
                System.out.print("|");
            }
            System.out.println();
        }

    }

    protected void printMapScore()
    {
        for (int i = 0; i < map.width; i++) {
            for (int j = 0; j < map.height; j++) {
                System.out.format("%04d", map.value[i][j]);
                System.out.print("|");
            }
            System.out.println();
        }

    }

}