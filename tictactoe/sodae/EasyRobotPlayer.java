package tictactoe.sodae;

import tictactoe.CellType;

import java.awt.*;
import java.util.*;

public class EasyRobotPlayer extends BaseRobotPlayer {

    final int MAP_ROW = 6;

    private Analyzer analyzerDefensive;
    private Analyzer analyzerOffensive;

    public EasyRobotPlayer(String name) {
        super(name);
        analyzerDefensive = new Analyzer(map, opposite, my, MAP_ROW);
        analyzerOffensive = new Analyzer(map, my, opposite, MAP_ROW);
    }

    @Override
    public void clean() {
        super.clean();
        analyzerDefensive = new Analyzer(map, opposite, my, MAP_ROW);
        analyzerOffensive = new Analyzer(map, my, opposite, MAP_ROW);
    }

    @Override
    public Point nextMove(Point move) {
        if (move == null) { // we start
            Point res = new Point(randInt(5, 15), randInt(5, 15)); // start, somewhere center;
            //Point res = new Point(14,8);
            map.setType(res.getX(), res.getY(), my);
            return res;
        }

        map.setType(move.getX(), move.getY(), opposite);

        for (int i = 0; i < map.width; i++) {
            for (int j = 0; j < map.height; j++) {
                map.value[i][j] = 0;
            }
        }

        valueMap(analyzerDefensive, 1);
        valueMap(analyzerOffensive, 0.6);

        int maxScore = 0;
        Point result = null;
        for (int i = 0; i < map.width; i++) {
            for (int j = 0; j < map.height; j++) {
                if (map.value[i][j] > maxScore) {
                    if (map.getType(i, j) == CellType.EMPTY) {
                        maxScore = map.value[i][j];
                        result = new Point(i, j);
                    }
                }
            }
        }

        map.setType(result.getX(), result.getY(), my);
        return result;
    }

    /**
     * xxxx = 10b
     * xxx = 7b
     * x???x{? = 2*x} - 8b
     * x???x{? = 1*x} - 5b
     *
     */

    protected void valueMap(Analyzer analyzer, double coefficient)
    {
        for (int i = 0; i < map.width; i++) {
            for (int j = 0; j < map.height; j++) {

                for (int si = -1; si <= 1; si++) {
                    for (int sj = -1; sj <= 1; sj++) {
                        int[] row = new int[MAP_ROW];
                        Arrays.fill(row, 0);
                        analyzer.mapRow(row, i, j, si, sj);
                        java.util.List<Analyzer.RowAnalyze> res = analyzer.analyzeRow(row);
                        for (Analyzer.RowAnalyze stringRow : res) {

                            int score = 0;
                            boolean mayInside = false;
                            if (stringRow.score == 4 && stringRow.length == 4) {
                                score = 10000;
                            } else if (stringRow.score <= 6 && stringRow.score >= 4 && stringRow.length > 4) {
                                score = 8100;
                                mayInside = true;
                            } else if (stringRow.score <= 6 && stringRow.score >= 3 && stringRow.length > 3) {
                                score = 5000;
                                mayInside = true;
                            } else if (stringRow.score == 3 && stringRow.length == 3) {
                                score = 1000;
                            } else if (stringRow.score >= 3 && stringRow.length > 3) {
                                score = 500;
                                mayInside = true;
                            } else if (stringRow.score == 2 && stringRow.length == 2) {
                                score = 300;
                            } else {
                                score = 1 * stringRow.score;
                            }

                            if (mayInside) {
                                for (int in = 0; in < stringRow.length; in++) {
                                    setValueOne(score, stringRow.offset + in, i, j, si, sj);
                                }
                            } else {
                                setValueOne((int) Math.ceil(score * coefficient), stringRow.offset + stringRow.length, i, j, si, sj);
                            }

                        }
                    }
                }
            }
        }
    }


    protected void setValueOne(int score, int length, int startX, int startY, int moveLeft, int moveDown) {
        int x = startX+(moveLeft*length);
        int y = startY+(moveDown*length);
        if ((moveLeft == 0 && moveDown == 0) || length == 0) return;
        if (x < 0 || y < 0 || x >= map.width || y >= map.height) return;
        map.value[x][y] += score;
    }


}
