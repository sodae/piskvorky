package tictactoe;

import java.awt.*;

public class UserPlayer implements IPlayer {

    private Point clicked;

    private String name;

    public UserPlayer(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void clean() {}

    @Override
    public Point nextMove(Point move) {
        while (true) {
            if (clicked != null) {
                Point r = clicked;
                clicked = null;
                return r;
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {

            }
        }
    }

    public void clicked(Point move) {
        clicked = move;
    }

}
