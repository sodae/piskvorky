/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tictactoe;

import tictactoe.sodae.EasyRobotPlayer;

import java.awt.*;

public class TicTacToe {

    protected static MainFrame frame;

    public static void main(String[] args) {
        Game game = new Game();
        initFrame(game);
        initConsole(game);
    }

    protected static void initFrame(Game game) {
        frame = new MainFrame(game);
        game.registerEventListener(new Game.Event() {
            @Override
            public void onStart(Game game) {
                frame.updateGame();
                frame.log("Hra začala");
            }

            @Override
            public void onFinish(Game game, IPlayer winner) {
                frame.winner(winner);
                frame.log("Vyhrál " + winner.getName());
            }

            @Override
            public void onDraw(Game game) {
                frame.onDraw();
                frame.log("Remíza");
            }

            @Override
            public void onBadMove(Game game, IPlayer player, Point move) {
                frame.onBadMove();
                frame.log("Neplatný tah " + player.getName() + " (" + move.getX() + "," + move.getY() + ")");
            }

            @Override
            public void onReset(Game game) {
                frame.updateGame();
                frame.log("Hra restartována");
            }

            @Override
            public void onMove(Game game, IPlayer player, Point move) {
                frame.updateGame();
                frame.log("Tah " + player.getName() + ": (x:" + move.getX() +  ", y:" + move.getY() + ")");
            }
        });
        frame.setVisible(true);

    }

    protected static void initConsole(Game game) {
        game.registerEventListener(new Game.Event() {
            @Override
            public void onStart(Game game) {
                System.out.println("Game started");
            }

            @Override
            public void onFinish(Game game, IPlayer winner) {
                System.out.println("WINNER: " + winner.getName());
            }

            @Override
            public void onDraw(Game game) {
                System.out.println("Remíza");
            }

            @Override
            public void onBadMove(Game game, IPlayer player, Point move) {
                System.out.println("Bad move " + player.getName() + " (" + move.getX() + "," + move.getY() + ")");
            }

            @Override
            public void onReset(Game game) {
                System.out.println("Game restarted");
            }

            @Override
            public void onMove(Game game, IPlayer player, Point move) {
                System.out.println("Move " + player.getName() + ": (x:" + move.getX() +  ", y:" + move.getY() + ")");
            }
        });
    }

}
