package tictactoe;

import tictactoe.sodae.BaseRobotPlayer;

import java.awt.*;

public class StupidPlayer extends BaseRobotPlayer {

    public StupidPlayer(String name) {
        super(name);
    }

    @Override
    public Point nextMove(Point move) {
        if (move != null)
            map.setType(move.x, move.y, opposite);
        while (true) {
            int x = randInt(0, WIDTH-1);
            int y = randInt(0, HEIGHT-1);
            if (map.isEmpty(x, y)) {
                map.setType(x, y, my);
                return new Point(x, y);
            }
        }
    }

}
