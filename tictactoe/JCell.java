package tictactoe;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

public class JCell extends JLabel {

    private Cell cell;

    public JCell(Cell cell) {
        this.cell = cell;
        this.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        this.setOpaque(true);
        this.setText(" ");
        update();
    }

    public Cell getCell() {
        return cell;
    }

    public void update() {
        CellType type = cell.getType();
        if (type == CellType.CIRCLE) {
            setBackground(Color.red);

            //URL iconURL = getClass().getResource("r/circle.png");
            //setIcon(new ImageIcon(iconURL));
        } else if (type == CellType.CROSS) {
            setBackground(Color.BLUE);

            //URL iconURL = getClass().getResource("r/cross.png");
            //setIcon(new ImageIcon(iconURL));
        } else {

            setIcon(null);
            setBackground(Color.white);
        }
    }
}
