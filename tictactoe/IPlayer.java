package tictactoe;

import java.awt.*;

public interface IPlayer {
    public String getName();
    public Point nextMove(Point move);
    public void clean();
}
